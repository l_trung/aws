module "computer" {
    source = "../modules/computer"
    common = "${var.common}"
    ec2 = "${var.ec2}"
    iam = "${var.iam}"
    alb = "${var.alb}"
    acm = "${var.acm}"
}