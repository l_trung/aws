variable "common" {
    type = "map"
    default = {
        region = "ap-northeast-1"
        vpc = "	vpc-856762e2"
        project = "NJG"
        role = "LOG"
        server_type = "COLLECTBATCH"
        creat_user = "_Creat_user_"
        key_name = "JOBGEAR"
        disable_api_termination = "true"
        ## tag
        CI = "true"
        vuls_scan = "true"
    }
}

variable "ec2" {
    type = "map"
    default = {
        ## dev
        dev.count = "1"
        dev.instance_type = "t2.micro"
        ## dev.mage_id = "ami-xxxxxxxxxxxxxxx"
        dev.subnet_id = "subnet-0bafc974b64dca404"
        dev.security_group_ids = "sg-0e6d0e384302e2981"

        ## EBS
        dev.volume_size = "10"
        dev.volume_type = "gp2"

        # tag
        dev.env = "dev"
        dev.backup_category = "3"
        dev.backup_generation = "1"
        dev.START = "default"
        dev.STOP = "default"

        ## prd
        prd.count = "1"
        prd.instance_type = "t2.micro"
        ## dev.mage_id = "ami-xxxxxxxxxxxxxxx"
        prd.subnet_id = "subnet-0bafc974b64dca404"
        prd.security_group_ids = "sg-0e6d0e384302e2981"

        ## EBS
        prd.volume_size = "10"
        prd.volume_type = "gp2"

        # tag
        prd.env = "dev"
        prd.backup_category = "3"
        prd.backup_generation = "1"
        prd.START = "default"
        prd.STOP = "default"
    }
}

variable "alb" {
    type = "map"
    default = {
        dev.count = "1"
        prd.count = "1"
    }
}

variable "acm" {
    type = "map"
    default = {

    }
}

variable "iam" {
    type = "map"
    default = {
        dev.role_name = "IAM-stg-njg-log-collectionbatch"
        prd.role_name = "IAM-stg-njg-log-collectionbatch"
    }
}

